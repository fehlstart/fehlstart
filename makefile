
PREFIX  = /usr/local
PKGS    = gtk+-3.0 keybinder-3.0
WFLAGS  = -Wall -Wno-unused-parameter -Wno-unused-result -Wno-deprecated-declarations
CFLAGS  = -O2 -pthread `pkg-config --cflags $(PKGS)`
LDFLAGS = -pthread `pkg-config --libs $(PKGS)` -lm

fehlstart: fehlstart.c
	$(CC) -o $@ $< $(WFLAGS) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) 

install: fehlstart
	install -Dsm 755 fehlstart $(PREFIX)/bin/fehlstart

clean:
	rm -f fehlstart

