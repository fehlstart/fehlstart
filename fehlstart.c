// Fehlstart - a small launcher written in C99.
// This file is distributed unter the GNU General Public License.

#define _DEFAULT_SOURCE // for readdir's DT_DIR

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <dirent.h>
#include <pthread.h>
#include <signal.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gdk/gdkkeysyms.h>
#include <gio/gdesktopappinfo.h>
#include <glib/gstdio.h>
#include <glib-unix.h>
#include <gtk/gtk.h>

#include <keybinder.h>

// types
typedef struct Action {
    char*  key;       // map key, .desktop file
    time_t file_time; // .desktop time stamp
    char*  name;      // display caption
    char*  exec;      // executable / hint
    char*  mnemonic;  // what user typed
    char*  icon;      // icon name or path
    int    score;     // calculated prority
    time_t time;      // last used timestamp
    bool   used;      // unused actions are cached to speed up rescans
    void (*action)(char*, struct Action*);
} Action;

// typedefs for settings
typedef char* string;
typedef bool  boolean;
typedef int   integer;

typedef struct Settings {
#define SETTING(type, group, key, value) type group##_##key;
#include "settings.def"
#undef SETTING
    bool one_time;
} Settings;

// forward declarations
static void  launch_action(char*, Action*);
static void  command_action(char*, Action*);
static void  edit_settings_action(char*, Action*);
static void  save_mnemonics(const char*, GHashTable*);
static void* update_all(void*);

// constants
#define WELCOME_MESSAGE       "..."
#define NO_MATCH_MESSAGE      "???"
#define APPLICATION_ICON      "applications-other"
#define DEFAULT_HOTKEY        "<Super>space"
#define DEFAULT_ICON          "system-search"
#define NO_MATCH_ICON         "dialog-question"
#define INPUT_STRING_SIZE     64
#define SHOW_IGNORE_TIME      100000
#define APPLICATIONS_DIR_0    "/usr/share/applications"
#define APPLICATIONS_DIR_1    "/usr/local/share/applications"
#define USER_APPLICATIONS_DIR ".local/share/applications"
#define HELP_MESSAGE          "fehlstart 0.5.1\noptions:\n\t--one-way\texit after one use\n"
#define ACTION_EXAMPLE                                                \
    "# When you launch a command, everything typed after the first "  \
    "space is passed to the command line as the last argument.\n"     \
    "# Run a command in XTerm\n#[Run in Terminal]\n#Exec=xterm -e\n"  \
    "#Icon=terminal\n\n# Use Firefox default search\n#[Search Web]\n" \
    "#Exec=firefox --search\n#Icon=firefox"

// preferences
static Settings settings = {
#define SETTING(type, group, name, value) .group##_##name = value,
#include "settings.def"
#undef SETTING
    .one_time = false,
};

// launcher stuff
static GHashTable*     action_map;
static GArray*         filter_list;
static unsigned        selection;
static char            input_string[INPUT_STRING_SIZE];
static pthread_mutex_t map_mutex;

// user interface
static GdkPixbuf*  icon_pixbuf;
static GtkWidget*  window;
static const char* action_name;
static bool        alpha_supported;
static int64_t     focus_out_time;

// files
static char* setting_file;
static char* mnemonic_file;
static char* commands_file;

//------------------------------------------
// helper functions

static bool is_readable_file(const char* file)
{
    FILE* f        = fopen(file, "r");
    bool  readable = f && fgetc(f) != EOF;
    if (f) fclose(f);
    return readable;
}

// updates mtime and returnes true if changed
static bool update_mtime(const char* file, time_t* mtime)
{
    struct stat st = {0};

    int  err     = stat(file, &st);
    bool changed = !err && st.st_mtime != *mtime;
    *mtime       = st.st_mtime;

    return changed;
}

static const char* home_dir(void)
{
    const char* home = getenv("HOME");
    if (!home) {
        home = g_get_home_dir();
    }
    return home;
}

// return length of first word
static size_t word_len(const char* s)
{
    char* sp = strchr(s, ' ');
    return sp ? (size_t)(sp - s) : strlen(s);
}

//------------------------------------------
// action functions

static void add_action(const char* name, const char* hint, const char* icon, void (*action)(char*, Action*))
{
    Action* a = g_new0(Action, 1);
    a->key    = g_strdup(name);
    a->name   = g_strdup(name);
    a->exec   = g_strdup(hint);
    a->icon   = g_strdup(icon);
    a->action = action;
    a->used   = true;
    g_hash_table_insert(action_map, a->key, a);
}

static void free_action(gpointer data)
{
    Action* a = data;
    g_free(a->key);
    g_free(a->name);
    g_free(a->exec);
    g_free(a->icon);
    g_free(a->mnemonic);
    g_free(a);
}

// todo: remove match executable flag
static void load_launcher(char* file, Action* action, bool match_executable)
{
    update_mtime(file, &action->file_time);
    action->action        = launch_action;
    GDesktopAppInfo* info = g_desktop_app_info_new_from_filename(file);
    if (!info) return;

    GAppInfo* app = G_APP_INFO(info);
    action->used  = !g_desktop_app_info_get_is_hidden(info) && g_app_info_should_show(app);

    if (action->used) {
        action->name = g_strdup(g_app_info_get_name(app));
        if (match_executable) {
            action->exec = g_strdup(g_app_info_get_executable(app));
        }
        GIcon* icon = g_app_info_get_icon(app);
        if (icon) {
            action->icon = g_icon_to_string(icon);
        }
    }

    g_object_unref(info);
}

static void reload_launcher(Action* action, bool match_executable)
{
    g_free(action->name);
    g_free(action->exec);
    g_free(action->icon);
    action->used = false;
    load_launcher(action->key, action, match_executable);
}

// taks ownership of file
static Action* new_launcher(char* file, bool match_executable)
{
    Action* a = g_new0(Action, 1);
    a->key    = g_strdup(file);
    load_launcher(file, a, match_executable);
    return a;
}

// updates launcher if file has changed
static void update_launcher(gpointer key, gpointer value, gpointer user_data)
{
    Action* a = value;
    if (a->action != launch_action) return;

    struct stat st;
    if (stat(a->key, &st)) {
        a->used = false; // stat fails if file doesn't exist
    } else if (a->file_time != st.st_mtime) {
        reload_launcher(a, settings.Matching_executable);
    }
}

// adds new launchers to action map, add operations are protected by map_mutex
static void add_launchers(char* dir_name)
{
    DIR* dir = opendir(dir_name);
    if (!dir) return;

    struct dirent* ent = NULL;
    while ((ent = readdir(dir))) {
        // create new path on stack
        char path[strlen(dir_name) + strlen(ent->d_name) + 2];
        g_stpcpy(g_stpcpy(g_stpcpy(path, dir_name), G_DIR_SEPARATOR_S), ent->d_name);

        // descent into sup-directories
        if (ent->d_type == DT_DIR && ent->d_name[0] != '.') {
            add_launchers(path);
            continue;
        }

        // skip non .desktop files
        char* d_ext = strrchr(ent->d_name, '.');
        if (!d_ext || g_ascii_strcasecmp(d_ext, ".desktop")) continue;

        // add to hash map unless it's already there
        if (!g_hash_table_contains(action_map, path)) {
            pthread_mutex_lock(&map_mutex);
            Action* a = new_launcher(path, settings.Matching_executable);
            g_hash_table_insert(action_map, a->key, a);
            pthread_mutex_unlock(&map_mutex);
        }
    }
    closedir(dir);
}

static void update_commands(void)
{
    static time_t commands_file_time;
    if (!update_mtime(commands_file, &commands_file_time)) return;

    // mark all command actions as unused
    gpointer       key   = NULL;
    gpointer       value = NULL;
    GHashTableIter iter  = {0};
    g_hash_table_iter_init(&iter, action_map);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        Action* a = value;
        if (a->action == command_action) a->used = false;
    }

    GKeyFile* kf = g_key_file_new();
    g_key_file_load_from_file(kf, commands_file, G_KEY_FILE_KEEP_COMMENTS, NULL);
    char** groups = g_key_file_get_groups(kf, NULL);

    // reload all commands
    for (int i = 0; groups[i]; i++) {
        char*   key = g_strconcat("!cmd:", groups[i], NULL);
        Action* a   = g_hash_table_lookup(action_map, key);
        if (a) {
            g_free(a->exec);
            g_free(a->icon);
            g_free(key);
        } else {
            a         = g_new0(Action, 1);
            a->action = command_action;
            a->name   = g_strdup(groups[i]);
            a->key    = key;
            pthread_mutex_lock(&map_mutex);
            g_hash_table_insert(action_map, key, a);
            pthread_mutex_unlock(&map_mutex);
        }
        a->exec = g_key_file_get_string(kf, groups[i], "Exec", NULL);
        a->icon = g_key_file_get_string(kf, groups[i], "Icon", NULL);
        a->used = true;
    }

    g_strfreev(groups);
    g_key_file_free(kf);
}

//------------------------------------------
// filter functions

// returns true if str begins with tst ignoring case. arguments are utf8, can be null
static bool begins(const char* restrict str, const char* restrict tst)
{
    if (!str || !tst) return false;
    while (*str && *tst) {
        gunichar a = g_unichar_tolower(g_utf8_get_char(str));
        gunichar t = g_unichar_tolower(g_utf8_get_char(tst));
        if (a != t) return false;
        str = g_utf8_next_char(str);
        tst = g_utf8_next_char(tst);
    }
    return !*tst;
}

// return nonzero if str contains tst ignoring case. arguments are utf8, can be null
static int contains(const char* restrict str, const char* restrict tst)
{
    if (!str || !tst) return 0;
    gunichar t = g_unichar_tolower(g_utf8_get_char(tst));
    for (int i = 1; *str; i++) {
        gunichar a = g_unichar_tolower(g_utf8_get_char(str));
        if (a == t && begins(str, tst)) return i;
        str = g_utf8_next_char(str);
    }
    return 0;
}

// calculate score of action based on filter, append to filter list
static void scrore(gpointer key, gpointer value, gpointer user_data)
{
    Action* a      = value;
    char*   filter = user_data;
    if (!a->used) return;

    a->score = -1;
    if (begins(a->mnemonic, filter)) a->score = 10000;
    if (a->score < 0) {
        int pos = contains(a->name, filter);
        if (pos) a->score = 1000 - MIN(pos, 99);
    }
    if (a->score < 0) {
        int pos = contains(a->exec, filter);
        if (pos) a->score = 100 - MIN(pos, 99);
    }
    if (a->score > 0) {
        if (a->score < 10000 && begins(filter, a->mnemonic)) a->score = 10000;
        g_array_append_val(filter_list, a);
    }
}

static int compare_score(gconstpointer a, gconstpointer b)
{
    Action* a1 = *(Action**)a;
    Action* a2 = *(Action**)b;
    return (a2->score - a1->score) + (a2->time < a1->time ? -1 : 1);
}

static void filter_action_list(char* filter)
{
    if (filter_list->len) g_array_remove_range(filter_list, 0, filter_list->len);
    if (strlen(filter) == 0) return;
    g_hash_table_foreach(action_map, scrore, filter);
    g_array_sort(filter_list, compare_score);
}

static void run_selected(void)
{
    if (!filter_list->len || selection >= filter_list->len) return;
    Action* a = g_array_index(filter_list, Action*, selection);
    if (!a->action) return;
    g_free(a->mnemonic);
    a->mnemonic = g_strndup(input_string, word_len(input_string));
    a->time     = time(NULL);
    a->action(input_string, a);
}

//------------------------------------------
// gui functions

static void rectangle(cairo_t* cr, double x, double y, double w, double h, double r)
{
    if (r > 0) {
        cairo_arc(cr, x + w - r, y + r, r, 1.5 * G_PI, 0);
        cairo_arc(cr, x + w - r, y + h - r, r, 0, 0.5 * G_PI);
        cairo_arc(cr, x + r, y + h - r, r, 0.5 * G_PI, G_PI);
        cairo_arc(cr, x + r, y + r, r, G_PI, 1.5 * G_PI);
        cairo_close_path(cr);
    } else {
        cairo_rectangle(cr, 0, 0, w, h);
    }
}

static void draw_labels(cairo_t* cr, Settings* set, GtkStyleContext* sty, const char* action, const char* input)
{
    cairo_text_extents_t extents;

    GdkRGBA c = {0, 0, 0, 0};
    if (!gdk_rgba_parse(&c, set->Labels_color)) {
        gtk_style_context_get_color(sty, GTK_STATE_FLAG_SELECTED, &c);
    }
    cairo_set_source_rgb(cr, c.red, c.green, c.blue);

    // if text is too large for window, decresase font size
    int max_width = set->Window_width - set->Border_width * 2;
    int size      = set->Labels_size1;
    do {
        cairo_set_font_size(cr, size--);
        cairo_text_extents(cr, action, &extents);
    } while (extents.width > max_width && size > 6);

    double x = (set->Window_width - extents.width) / 2.0;
    double y = set->Window_height * 0.75;
    cairo_move_to(cr, x, y);
    cairo_show_text(cr, action);

    if (set->Labels_showinput) {
        cairo_set_font_size(cr, set->Labels_size2);
        cairo_text_extents(cr, input, &extents);
        x = (set->Window_width - extents.width) / 2.0;
        y = set->Window_height - set->Border_width * 2.0;
        cairo_move_to(cr, x, y);
        cairo_show_text(cr, input);
    }
}

static void draw_icon(cairo_t* cr, Settings* set, GdkPixbuf* icon)
{
    if (!icon) return;
    double x = (set->Window_width - gdk_pixbuf_get_width(icon)) / 2.0;
    double y = fmax(set->Window_height / 2.0 - gdk_pixbuf_get_height(icon), set->Border_width);
    gdk_cairo_set_source_pixbuf(cr, icon, x, y);
    cairo_paint(cr);
}

static void draw_dots(cairo_t* cr, Settings* set, GtkStyleContext* sty, int index, int max)
{
    if (max < 1) return;
    double  r = set->Window_width / 100.0; // circle radius
    double  w = set->Window_width;
    double  y = set->Window_height / 2.0;
    GdkRGBA c = {0, 0, 0, 0};
    if (!gdk_rgba_parse(&c, set->Labels_color)) {
        gtk_style_context_get_color(sty, GTK_STATE_FLAG_SELECTED, &c);
    }

    for (int i = 0; i < MIN(3, index); i++) {
        cairo_arc(cr, r * 3 * (i + 1), y, r, 0, 2 * G_PI);
    }
    for (int i = 0; i < MIN(3, max - index - 1); i++) {
        cairo_arc(cr, w - (r * 3 * (i + 1)), y, r, 0, 2 * G_PI);
    }
    cairo_close_path(cr);
    cairo_set_source_rgb(cr, c.red, c.green, c.blue);
    cairo_fill(cr);
}

static void draw_window(cairo_t* cr, Settings* set, GtkStyleContext* sty)
{
    double w    = set->Window_width;
    double h    = set->Window_height;
    double brad = set->Window_round && alpha_supported ? fmax(w, h) / 10 : 0; // corner radius

    // draw background
    rectangle(cr, 0, 0, w, h, brad);
    cairo_clip(cr);
    gtk_render_background(sty, cr, 0, 0, w, h);

    // draw background highlight
    if (set->Window_arch) {
        double w2 = w / 2, h3 = w * 3;
        double crad = sqrt(w2 * w2 + h3 * h3);
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, w, 0);
        // todo figure out correct angles
        cairo_arc_negative(cr, w2, h * 0.6 + h3, crad, 0, G_PI);
        cairo_close_path(cr);
        cairo_set_source_rgba(cr, 1, 1, 1, 0.2);
        cairo_fill(cr);
    }

    // draw border
    GdkRGBA c = {0, 0, 0, 0};
    if (!gdk_rgba_parse(&c, set->Border_color)) {
        gtk_style_context_get_color(sty, GTK_STATE_FLAG_SELECTED, &c);
    }
    rectangle(cr, 0, 0, w, h, brad);
    cairo_set_line_width(cr, set->Border_width * 2);
    cairo_set_source_rgb(cr, c.red, c.green, c.blue);
    cairo_stroke(cr);
}

static void clear(cairo_t* cr)
{
    cairo_set_source_rgba(cr, 0, 0, 0, 0);
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_paint(cr);
    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
}

static gboolean draw(GtkWidget* widget, GdkEvent* event, gpointer data)
{
    cairo_t*         cr  = gdk_cairo_create(gtk_widget_get_window(widget));
    GtkStyleContext* sty = gtk_widget_get_style_context(window);
    gtk_style_context_save(sty);
    gtk_style_context_add_class(sty, GTK_STYLE_CLASS_VIEW);
    gtk_style_context_set_state(sty, GTK_STATE_FLAG_SELECTED);

    clear(cr);
    draw_window(cr, &settings, sty);
    draw_icon(cr, &settings, icon_pixbuf);
    draw_dots(cr, &settings, sty, selection, filter_list->len);
    draw_labels(cr, &settings, sty, action_name, input_string);

    cairo_destroy(cr);
    gtk_style_context_restore(sty);
    return false;
}

static GdkPixbuf* load_icon(const char* name, Settings* set)
{
    const int sizes[] = {256, 128, 48, 32};
    if (!set->Icons_show || !name) return NULL;
    // if the size is uncommon, svgs might be used which load too slow on my atom machine
    int h = set->Window_height / 2;
    if (!set->Icons_scale) {
        int i = 0;
        for (; i < (int)G_N_ELEMENTS(sizes) && h < sizes[i]; i++);
        h = sizes[i];
    }
    if (g_path_is_absolute(name)) {
        return gdk_pixbuf_new_from_file_at_scale(name, -1, h, true, NULL);
    } else {
        int           flags = GTK_ICON_LOOKUP_FORCE_SIZE | GTK_ICON_LOOKUP_USE_BUILTIN;
        GtkIconTheme* theme = gtk_icon_theme_get_default();
        return gtk_icon_theme_load_icon(theme, name, h, flags, NULL);
    }
}

static void show_selected(void)
{
    const char* icon_name = NO_MATCH_ICON;
    action_name           = NO_MATCH_MESSAGE;

    if (strlen(input_string) == 0) {
        action_name = WELCOME_MESSAGE;
        icon_name   = DEFAULT_ICON;
    } else if (filter_list->len > 0) {
        Action* a   = g_array_index(filter_list, Action*, selection);
        action_name = a->name;
        icon_name   = a->icon;
    }

    if (icon_pixbuf) g_object_unref(icon_pixbuf);
    icon_pixbuf = load_icon(icon_name, &settings);
    gtk_widget_queue_draw(window);
}

static void handle_text_input(GdkEventKey* event)
{
    guint32 uc   = gdk_keyval_to_unicode(event->keyval);
    size_t  size = strlen(input_string);

    if (event->keyval == GDK_KEY_BackSpace && size > 0) {
        *g_utf8_find_prev_char(input_string, input_string + size) = 0;
    } else if (g_unichar_isprint(uc)) {
        char utfch[8] = {0};
        int  len      = g_unichar_to_utf8(uc, utfch);
        if (size + len < INPUT_STRING_SIZE) {
            g_strlcat(input_string, utfch, INPUT_STRING_SIZE);
        }
    }

    size_t wlen = MIN(word_len(input_string) + 1, INPUT_STRING_SIZE);
    char   filter[wlen];
    g_strlcpy(filter, input_string, wlen);
    filter_action_list(filter);
    selection = 0;
}

static void hide_window(void)
{
    if (!gtk_widget_get_visible(window)) return;
    if (settings.one_time) gtk_main_quit();

    gtk_widget_hide(window);
    input_string[0] = 0;
    selection       = 0;
    if (filter_list->len) {
        g_array_remove_range(filter_list, 0, filter_list->len);
    }
}

static void show_window(void)
{
    if (gtk_widget_get_visible(window)) return;
    show_selected();
    gtk_widget_set_size_request(window, settings.Window_width, settings.Window_height);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER_ALWAYS);
    gtk_window_present(GTK_WINDOW(window));
}

static void hotkey_handler(const char* keystring, void* data)
{
    if (gtk_widget_get_visible(window)) {
        hide_window();
    } else if (g_get_monotonic_time() - focus_out_time > 100000) {
        // the hotkey handler triggeres a focus-out event. this workaround only shows
        // the window if the last focus-out was at least 100 ms ago.
        pthread_t thread = 0;
        pthread_create(&thread, NULL, update_all, NULL);
        pthread_detach(thread);
        show_window();
    }
}

static void destroy(GtkWidget* widget, gpointer data)
{
    gtk_main_quit();
}

static gboolean key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer data)
{
    pthread_mutex_lock(&map_mutex);
    switch (event->keyval) {
    case GDK_KEY_Escape:
        hide_window();
        break;
    case GDK_KEY_KP_Enter:
    case GDK_KEY_Return:
        run_selected();
        hide_window();
        save_mnemonics(mnemonic_file, action_map);
        break;
    case GDK_KEY_Left:
    case GDK_KEY_Up:
        if (filter_list->len) {
            selection = (selection + filter_list->len - 1) % filter_list->len;
        }
        show_selected();
        break;
    case GDK_KEY_Right:
    case GDK_KEY_Tab:
    case GDK_KEY_Down:
        if (filter_list->len) {
            selection = (selection + 1) % filter_list->len;
        }
        show_selected();
        break;
    default:
        handle_text_input(event);
        show_selected();
        break;
    }
    pthread_mutex_unlock(&map_mutex);
    return true;
}

static gboolean focus_out_event(GtkWidget* widget, GdkEvent* event, gpointer user_data)
{
    focus_out_time = g_get_monotonic_time();
    hide_window();
    return true;
}

static void screen_changed(GtkWidget* widget, GdkScreen* old_screen, gpointer userdata)
{
    GdkScreen* screen = gtk_widget_get_screen(widget);
    GdkVisual* visual = gdk_screen_get_rgba_visual(screen);
    alpha_supported   = visual != NULL;
    if (alpha_supported) gtk_widget_set_visual(widget, visual);
}

static void create_widgets(void)
{
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_app_paintable(window, true);
    gtk_window_set_resizable(GTK_WINDOW(window), false);
    gtk_window_set_decorated(GTK_WINDOW(window), false);
    gtk_window_set_skip_pager_hint(GTK_WINDOW(window), true);
    gtk_window_set_skip_taskbar_hint(GTK_WINDOW(window), true);
    gtk_window_set_accept_focus(GTK_WINDOW(window), true);
    gtk_window_set_keep_above(GTK_WINDOW(window), true);
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK | GDK_FOCUS_CHANGE_MASK);

    g_signal_connect(window, "key-press-event", G_CALLBACK(key_press_event), NULL);
    g_signal_connect(window, "focus-out-event", G_CALLBACK(focus_out_event), NULL);
    g_signal_connect(window, "screen-changed", G_CALLBACK(screen_changed), NULL);
    g_signal_connect(window, "draw", G_CALLBACK(draw), NULL);
    g_signal_connect(window, "destroy", G_CALLBACK(destroy), NULL);
    screen_changed(window, NULL, NULL);
}

//------------------------------------------
// settings, etc...

static void key_file_save(GKeyFile* kf, const char* file_name)
{
    FILE* f = fopen(file_name, "w");
    if (!f) return;
    gsize length = 0;
    char* data   = g_key_file_to_data(kf, &length, NULL);
    fwrite(data, 1, length, f);
    g_free(data);
    fclose(f);
}

static void load_settings(const char* file_name, Settings* set)
{
    static time_t config_file_time;
    if (!update_mtime(file_name, &config_file_time)) return;

    GKeyFile* kf = g_key_file_new();
    if (g_key_file_load_from_file(kf, file_name, G_KEY_FILE_NONE, NULL)) {
#define SETTING(type, group, key, value) \
    if (g_key_file_has_key(kf, #group, #key, NULL)) set->group##_##key = g_key_file_get_##type(kf, #group, #key, NULL);
#include "settings.def"
#undef SETTING
    }
    g_key_file_free(kf);
    set->Border_width  = CLAMP(set->Border_width, 0, 20);
    set->Window_width  = CLAMP(set->Window_width, 200, 800);
    set->Window_height = CLAMP(set->Window_height, 100, 800);
    set->Labels_size1  = CLAMP(set->Labels_size1, 6, 32);
    set->Labels_size2  = CLAMP(set->Labels_size2, 6, 32);
}

static void save_settings(const char* file_name, Settings* set)
{
    GKeyFile* kf = g_key_file_new();
    g_key_file_load_from_file(kf, file_name, G_KEY_FILE_KEEP_COMMENTS, NULL);
#define SETTING(type, group, key, value) g_key_file_set_##type(kf, #group, #key, set->group##_##key);
#include "settings.def"
#undef SETTING
    key_file_save(kf, file_name);
    g_key_file_free(kf);
}

static void save_mnemonics(const char* file_name, GHashTable* map)
{
    GKeyFile*      kf  = g_key_file_new();
    gpointer       key = NULL, value = NULL;
    GHashTableIter iter;
    g_hash_table_iter_init(&iter, map);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        Action* a = value;
        if (a->mnemonic && strlen(a->mnemonic) > 0) {
            g_key_file_set_string(kf, a->key, "mnemonic", a->mnemonic);
            g_key_file_set_uint64(kf, a->key, "time", (uint64_t)a->time);
        }
    }
    key_file_save(kf, file_name);
    g_key_file_free(kf);
}

// actions should be populated with update_all() before
static void load_mnemonics(const char* file_name, GHashTable* map)
{
    GKeyFile* kf = g_key_file_new();
    if (g_key_file_load_from_file(kf, file_name, G_KEY_FILE_NONE, NULL)) {
        char** groups = g_key_file_get_groups(kf, NULL);
        for (unsigned i = 0; groups[i]; i++) {
            Action* a = g_hash_table_lookup(map, groups[i]);
            if (!a) continue;
            a->mnemonic = g_key_file_get_string(kf, groups[i], "mnemonic", NULL);
            a->time     = (time_t)g_key_file_get_uint64(kf, groups[i], "time", NULL);
        }
        g_strfreev(groups);
    }
    g_key_file_free(kf);
}

static void* update_all(void* user_data)
{
    load_settings(setting_file, &settings);
    update_commands();
    g_hash_table_foreach(action_map, update_launcher, NULL);
    add_launchers(APPLICATIONS_DIR_0);
    add_launchers(APPLICATIONS_DIR_1);
    add_launchers(USER_APPLICATIONS_DIR);
    return NULL;
}

// fork, setsid, restore SIGCHLD, return true if child
static bool fork_child(void)
{
    pid_t pid = fork();
    if (pid != 0) return false;
    setsid();                 // leave parent
    signal(SIGCHLD, SIG_DFL); // go back to default child behaviour
    return true;
}

// opens file in an editor
static void run_editor(const char* file)
{
    if (!is_readable_file(file)) return;
    if (!fork_child()) return;
    execlp("xdg-open", "", file, (char*)0);
    execlp("x-terminal-emulator", "", "-e", "editor", file, (char*)0);
    execlp("xterm", "", "-e", "vi", file, (char*)0); // getting desperate
    printf("failed to open editor for %s\n", file);
    _exit(0);
}

//------------------------------------------
// actions

static void quit_action(char* command, Action* action)
{
    gtk_main_quit();
}

static void spawn_setup(gpointer user_data)
{
    setsid();
}

static void launch_action(char* command, Action* action)
{
    GDesktopAppInfo* info = g_desktop_app_info_new_from_filename(action->key);
    if (!info) return;
    // g_app_info_launch doesn't perform setsid
    g_desktop_app_info_launch_uris_as_manager(info, NULL, NULL, G_SPAWN_SEARCH_PATH, spawn_setup, NULL, NULL, NULL,
                                              NULL);
    g_object_unref(info);
}

static void command_action(char* command, Action* action)
{
    if (!fork_child()) return;
    char* arg = strchr(command, ' '); // everything after first space is argument
    char* cmd = g_strconcat(action->exec, arg ? arg : "", NULL);
    execl("/bin/sh", "sh", "-c", cmd, (char*)0);
    _exit(0);
}

static void edit_settings_action(char* command, Action* action)
{
    save_settings(setting_file, &settings);
    run_editor(setting_file);
}

static void edit_commands_action(char* command, Action* action)
{
    if (!is_readable_file(commands_file)) {
        FILE* f = fopen(commands_file, "w");
        if (!f) return;
        fputs(ACTION_EXAMPLE, f);
        fclose(f);
    }
    run_editor(commands_file);
}

//------------------------------------------
// misc

static void parse_commandline(int argc, char** argv, Settings* set)
{
    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--one-way")) {
            set->one_time = true;
        } else if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
            printf(HELP_MESSAGE);
            exit(EXIT_SUCCESS);
        } else {
            printf("invalid option: %s\n", argv[i]);
        }
    }
}

static void register_hotkey(const char* binding)
{
    keybinder_init();
    if (!keybinder_bind(binding, hotkey_handler, NULL)) {
        GtkWidget* dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
                                                   GTK_BUTTONS_NONE, "Hotkey '%s' is already being used!", binding);
        gtk_dialog_add_buttons(GTK_DIALOG(dialog), "Edit Settings", GTK_RESPONSE_ACCEPT, "Cancel", GTK_RESPONSE_REJECT,
                               NULL);
        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
            edit_settings_action("", NULL);
        }
        gtk_widget_destroy(dialog);
        _exit(1);
    }
}

static int signal_quit(void* user_data)
{
    gtk_main_quit();
    return G_SOURCE_CONTINUE;
};

static int signal_show(void* user_data)
{
    show_window();
    return G_SOURCE_CONTINUE;
}

// if fehlstart is already running, send SIGUSR1 to that process
static void check_pidfile(const char* path)
{
    pid_t pid = getpid();
    int   fd  = open(path, O_CREAT | O_RDWR, 0666);
    int   rc  = lockf(fd, F_TLOCK, 0);
    if (rc == 0) {
        write(fd, &pid, sizeof pid);
    } else {
        rc = (errno == EACCES || errno == EAGAIN) && read(fd, &pid, sizeof pid) > 0 && kill(pid, SIGUSR1) == 0;
        _exit(!rc);
    }
}

//------------------------------------------
// main

int main(int argc, char** argv)
{
    // files & directories
    g_chdir(home_dir());
    char* conf_dir = g_build_filename(g_get_user_config_dir(), "fehlstart", NULL);
    char* pid_file = g_build_filename(conf_dir, "fehlstart.pid", NULL);
    setting_file   = g_build_filename(conf_dir, "fehlstart.rc", NULL);
    commands_file  = g_build_filename(conf_dir, "commands.rc", NULL);
    mnemonic_file  = g_build_filename(conf_dir, "actions.rc", NULL);
    g_mkdir_with_parents(conf_dir, 0700);

    // handle running instances, init stuff
    check_pidfile(pid_file);
    gtk_init(&argc, &argv);
    parse_commandline(argc, argv, &settings);

    // init data structures
    action_map  = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, free_action);
    filter_list = g_array_sized_new(false, true, sizeof(Action*), 200);
    add_action("Quit Fehlstart", "exit", "application-exit", quit_action);
    add_action("Fehlstart Actions", "commands", "system-run", edit_commands_action);
    add_action("Fehlstart Settings", "config preferences", "preferences-system", edit_settings_action);

    // load data
    update_all(NULL);
    load_mnemonics(mnemonic_file, action_map);

    // signals
    g_unix_signal_add(SIGTERM, signal_quit, NULL);
    g_unix_signal_add(SIGINT, signal_quit, NULL);
    g_unix_signal_add(SIGUSR1, signal_show, NULL);
    signal(SIGCHLD, SIG_IGN); // kernel reaps children

    create_widgets();
    if (settings.one_time) {
        show_window();
    } else {
        register_hotkey(settings.Bindings_launch);
    }
    gtk_main();
}
